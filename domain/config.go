package domain

import (
	"time"
)

type Config struct {
	Project  Project  `mapstructure:"project"`
	Provider Provider `mapstructure:"provider"`
	Metrics  Metrics  `mapstructure:"metrics"`
	HTTP     HTTP     `mapstructure:"http"`
	GRPC     GRPC     `mapstructure:"grpc"`
	Cors     string   `mapstructure:"cors"`
}

type Project struct {
	Name string `mapstructure:"name"`
	Log  Log    `mapstructure:"log"`
}

type Log struct {
	Level string `mapstructure:"level"`
}

type Provider struct {
	PSQL  PSQL  `mapstructure:"psql"`
	Redis Redis `mapstructure:"redis"`
}

type Redis struct {
	DSN string `mapstructure:"dsn"`
}

type PSQL struct {
	DSN          string        `mapstructure:"dsn"`
	ReadTimeout  time.Duration `mapstructure:"read_timeout"`
	WriteTimeout time.Duration `mapstructure:"write_timeout"`
}

type Metrics struct {
	Host string `mapstructure:"host"`
	Port string `mapstructure:"port"`
}

type HTTP struct {
	Host         string        `mapstructure:"host"`
	Port         string        `mapstructure:"port"`
	WriteTimeout time.Duration `mapstructure:"write_timeout"`
	ReadTimeout  time.Duration `mapstructure:"read_timeout"`
	IdleTimeout  time.Duration `mapstructure:"idle_timeout"`
	SSLSertPath  string        `mapstructure:"ssl_web_path"`
	SSLKeyPath   string        `mapstructure:"ssl_key_path"`
}

type GRPC struct {
	Host         string        `mapstructure:"host"`
	Port         string        `mapstructure:"port"`
	WriteTimeout time.Duration `mapstructure:"write_timeout"`
	ReadTimeout  time.Duration `mapstructure:"read_timeout"`
	IdleTimeout  time.Duration `mapstructure:"idle_timeout"`
}
