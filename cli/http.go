package cli

import (
	"context"
	"database/sql"
	"net"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"github.com/pressly/goose/v3"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/cobra"
	"github.com/uptrace/bun/driver/pgdriver"
	"gitlab.com/cadyrov/boilerplate"
	"gitlab.com/cadyrov/boilerplate/api/grpc/v1"
	"gitlab.com/cadyrov/boilerplate/api/http/handlers"
	"gitlab.com/cadyrov/boilerplate/api/http/router"
	"golang.org/x/exp/slog"
	"google.golang.org/grpc"
)

// nolint: gochecknoglobals
var (
	httpCmd = &cobra.Command{
		Use:   "http",
		Short: "run http server",
		Long:  `init http and grpc server`,
		Run: func(cmd *cobra.Command, args []string) {
			initHttpserver()
		},
	}
)

// nolint: gochecknoinits
func init() {
	rootCmd.AddCommand(httpCmd)

	cobra.OnInitialize(initConfig)
}

// nolint: funlen
func initHttpserver() {
	c := cfg

	_ = c

	serv, err := initService()
	if err != nil {
		slog.Error("cli init service", err)

		os.Exit(1)
	}

	hdl := handlers.New(serv, cfg)

	route, err := router.New(hdl)
	if err != nil {
		slog.Error("cli init service", err)
		os.Exit(1)
	}

	stopWaitGroup := &sync.WaitGroup{}

	ctx, cancel := context.WithCancel(context.Background())

	stopWaitGroup.Add(1)

	rt := mux.NewRouter()
	rt.Handle("/metrics", promhttp.Handler())

	metricsHTTPServer := http.Server{
		Handler:           rt,
		Addr:              cfg.Metrics.Host + ":" + cfg.Metrics.Port,
		ReadHeaderTimeout: cfg.HTTP.ReadTimeout,
		ReadTimeout:       cfg.HTTP.ReadTimeout,
		WriteTimeout:      cfg.HTTP.WriteTimeout,
		IdleTimeout:       cfg.HTTP.IdleTimeout,
	}

	go func() {
		defer stopWaitGroup.Done()
		slog.Debug("serving metrics ", cfg.Metrics.Host, cfg.Metrics.Port)

		if err := metricsHTTPServer.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
			slog.Error("metrics serving", err)
		}
	}()

	srv := &http.Server{
		Handler:           route.Routes(),
		Addr:              cfg.HTTP.Host + ":" + cfg.HTTP.Port,
		ReadTimeout:       cfg.HTTP.ReadTimeout,
		WriteTimeout:      cfg.HTTP.WriteTimeout,
		IdleTimeout:       cfg.HTTP.IdleTimeout,
		ReadHeaderTimeout: cfg.HTTP.ReadTimeout,
	}

	stopWaitGroup.Add(1)

	go func() {
		defer stopWaitGroup.Done()
		slog.Debug("serving http", err, cfg.HTTP.Host, cfg.HTTP.Port)

		if cfg.HTTP.SSLSertPath != "" && cfg.HTTP.SSLKeyPath != "" {
			if err := srv.ListenAndServeTLS(cfg.HTTP.SSLSertPath, cfg.HTTP.SSLKeyPath); err != nil {
				slog.Error("http serving error", err)
			}

			return
		}

		if err := srv.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
			slog.Error("http serving error", err)
		}
	}()

	gsrv := grpc.NewServer()
	v1.RegisterCheckServer(gsrv, serv)

	grpcListener, err := net.Listen("tcp", cfg.GRPC.Host+":"+cfg.GRPC.Port)
	if err != nil {
		slog.Error("grpc serving", err)
		cancel()

		return
	}

	stopWaitGroup.Add(1)

	go func() {
		defer stopWaitGroup.Done()
		slog.Debug("serving grpc", err, cfg.GRPC.Host, cfg.GRPC.Port)

		err = gsrv.Serve(grpcListener)
		if err != nil {
			slog.Error("grpc serving", err)
		}
	}()

	stopWaitGroup.Add(1)

	SigTermHandler(func() {
		defer stopWaitGroup.Done()
		if err := metricsHTTPServer.Shutdown(ctx); err != nil {
			slog.Error("metrics service shutdown", err)
		}

		if err := srv.Shutdown(ctx); err != nil {
			slog.Error("http service shutdown", err)
		}

		gsrv.GracefulStop()

		cancel()
	})

	stopWaitGroup.Wait()
}

const maxTermChanLen = 10

func SigTermHandler(stopFunc func()) {
	termCh := make(chan os.Signal, maxTermChanLen)
	signal.Notify(termCh, syscall.SIGTERM)

	go func() {
		<-termCh

		stopFunc()
	}()
}

func migrate(dsn string) {
	goose.SetBaseFS(boilerplate.EmbedMigrations)

	if err := goose.SetDialect("postgres"); err != nil {
		panic(err)
	}

	db := sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(dsn)))

	if err := goose.Up(db, "resources/migrations"); err != nil {
		panic(err)
	}
}
