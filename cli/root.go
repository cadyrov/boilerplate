package cli

import (
	"context"
	"fmt"
	"os"
	"strings"

	"github.com/go-redis/redis/v8"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/cadyrov/boilerplate/domain"
	"gitlab.com/cadyrov/boilerplate/internal/service"
	"gitlab.com/cadyrov/boilerplate/internal/storage/cache"
	"gitlab.com/cadyrov/boilerplate/internal/storage/psql"
)

// nolint: gochecknoglobals
var (
	cfgFileName = "config"
	cfgFileType = "yaml"
	cfgFilePath = "./resources"
	envPrefix   = ""
	cfg         = domain.Config{}

	rootCmd = &cobra.Command{
		Use:   "root",
		Short: "root",
		Long:  `root`,
		Run:   func(cmd *cobra.Command, args []string) {},
	}
)

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

// nolint:gochecknoinits
func init() {
}

func initService() (*service.Service, error) {
	storage, err := psql.New(cfg.Provider.PSQL.DSN, cfg.Provider.PSQL.ReadTimeout,
		cfg.Provider.PSQL.WriteTimeout)
	if err != nil {
		return nil, errors.Wrap(err, "init service")
	}

	migrate(cfg.Provider.PSQL.DSN)

	redisOpts, err := redis.ParseURL(cfg.Provider.Redis.DSN)
	if err != nil {
		return nil, errors.Wrap(err, "init service")
	}

	ch := cache.New(cfg, redis.NewClient(redisOpts))
	if err = ch.Ping(context.Background()); err != nil {
		return nil, errors.Wrap(err, "init service")
	}

	srv, err := service.New(storage, ch)

	return srv, errors.Wrap(err, "init service")
}

func initConfig() {
	v := viper.New()

	v.SetConfigName(cfgFileName)

	v.SetConfigType(cfgFileType)

	v.AddConfigPath(cfgFilePath)

	var errViperCNF *viper.ConfigFileNotFoundError
	if err := v.ReadInConfig(); err != nil {
		// It's okay if there isn't a config file
		if errors.As(err, &errViperCNF) {
			panic(err)
		}
	}

	v.AllowEmptyEnv(true)

	v.SetEnvPrefix(envPrefix)

	v.AutomaticEnv()

	r := strings.NewReplacer(".", "_")

	v.SetEnvKeyReplacer(r)

	if err := v.Unmarshal(&cfg); err != nil {
		panic(err)
	}
}
