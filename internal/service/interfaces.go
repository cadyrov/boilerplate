package service

import (
	"context"
)

type Cache interface {
	Ping(ctx context.Context) error
}

type Storage interface {
	Ping(ctx context.Context) error
}
