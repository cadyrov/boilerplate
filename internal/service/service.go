package service

import (
	"gitlab.com/cadyrov/boilerplate/api/grpc/v1"
)

type Service struct {
	storage Storage
	cache   Cache

	v1.CheckServer
}

func New(storage Storage, cache Cache) (*Service, error) {
	s := &Service{storage: storage, cache: cache}

	return s, nil
}
