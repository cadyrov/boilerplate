package psql

import (
	"context"
	"database/sql"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"
	"gitlab.com/cadyrov/goerr"
)

type Storage struct {
	readTimeout  time.Duration
	writeTimeout time.Duration
	dsn          string
	db           *bun.DB
}

var ErrSetDsn = goerr.BadRequest{}.Wrap(errors.New("set dsn"), "storage psql")

func (s Storage) Ping(ctx context.Context) error {
	if err := s.db.Ping(); err != nil {
		return goerr.Internal{}.Wrap(err, "storage psql")
	}

	return nil
}

func New(dsn string, read, write time.Duration) (*Storage, error) {
	if strings.Trim(dsn, " ") == "" {
		return nil, ErrSetDsn
	}

	storage := &Storage{dsn: dsn, readTimeout: read, writeTimeout: write}

	sqlDB := sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(dsn)))

	db := bun.NewDB(sqlDB, pgdialect.New())

	storage.db = db

	if e := storage.Ping(context.Background()); e != nil {
		return nil, goerr.Internal{}.Wrap(e, "init psql storage")
	}

	return storage, nil
}
