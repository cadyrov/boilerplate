package cache

import (
	"context"

	"github.com/go-redis/redis/v8"
	"github.com/pkg/errors"
	"gitlab.com/cadyrov/boilerplate/domain"
)

type Cache struct {
	config domain.Config
	redis  *redis.Client
}

func New(config domain.Config, redis *redis.Client) *Cache {
	return &Cache{
		config: config,
		redis:  redis,
	}
}

func (c *Cache) Ping(ctx context.Context) error {
	return errors.Wrap(c.redis.Ping(ctx).Err(), "ping")
}
