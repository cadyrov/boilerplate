#!/usr/bin/env bash 
user=
token=
registry=registry.gitlab.com/cadyrov/boilerplate:main
img=registry.gitlab.com/cadyrov/boilerplate
file=/var/boilerplate/.env

export `cat ${file}`

docker login -u ${user} -p ${token} ${registry}
docker pull ${registry}

docker system prune -f

imagehashfromcontainer=$(docker inspect boilerplate --format='{{.Image}}'| grep sha)
imgId=$(docker images | grep $img | awk '{print $3}')
imagehashfromregistry=$(docker inspect $imgId --format='{{.Id}}'| grep sha)

echo "imagehashfromcontainer" ${imagehashfromcontainer}
echo "imgId" ${imgId}
echo "imagehashfromregistry" ${imagehashfromregistry}

echo "imagehashfromregistry" $WEB_PORT

if [ "$imagehashfromcontainer" != "$imagehashfromregistry" ]
then docker stop boilerplate
docker system prune -f
docker run -d --name boilerplate -v /etc/letsencrypt/:/etc/letsencrypt -v /var/boilerplate/files:/files -p $HTTP_PORT:$HTTP_PORT -p $METRICS_PORT:$METRICS_PORT  -p GRPC_PORT:GRPC_PORT --env-file=${file} ${registry}
echo "Docker re-run"
exit
fi

if [ "$imagehashfromcontainer" == "" ]
then docker stop boilerplate
docker system prune -f
docker run -d --name boilerplate -v /etc/letsencrypt/:/etc/letsencrypt -v /var/boilerplate/files:/files -p $HTTP_PORT:$HTTP_PORT -p $METRICS_PORT:$METRICS_PORT   -p GRPC_PORT:GRPC_PORT --env-file=${file} ${registry}
echo "Docker re-run"
fi