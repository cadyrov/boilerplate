build:
	go build -o ./bin/srv ./cmd_endpoint/main.go
lint:
	gofmt -s -w ./ && golangci-lint run ./...
migrate-up:
	export `cat local.env` && GOOSE_DRIVER=postgres goose -dir=provider/storage/psql/migrations up
migrate-down:
	export `cat local.env` && GOOSE_DRIVER=postgres goose -dir=provider/storage/psql/migrations down
migrate-create:
	@read -p "Enter migration name: " name; \
	export `cat local.env` && GOOSE_DRIVER=postgres goose -dir=provider/storage/psql/migrations create $$name sql
swagger:
	rm -f ./resources/oas.json && \
	go-swagger3 --module-path . --main-file-path  ./api/http/apidoc.go --output ./resources/oas.json --schema-without-pkg --generate-yaml true
run:
	export `cat local.env` && ./bin/srv http
test:
	go test -v ./...
proto-gen:
	protoc \
    	-I=${GOPATH}/pkg/mod/github.com/envoyproxy/protoc-gen-validate@v0.6.7 \
    	--proto_path=proto/v1/ \
    	--go_out=. \
    	--go-grpc_out=. \
    	--validate_out="lang=go:." \
    	proto/v1/*.proto
