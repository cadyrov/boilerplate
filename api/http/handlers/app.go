package handlers

import (
	"gitlab.com/cadyrov/boilerplate/domain"
	"gitlab.com/cadyrov/boilerplate/internal/service"
)

type App struct {
	Service *service.Service
	config  domain.Config
}

func New(srv *service.Service, cnf domain.Config) *App {
	app := &App{
		Service: srv,
		config:  cnf,
	}

	return app
}
