package handlers

import (
	"log"
	"net/http"
	"runtime/debug"
	"strings"
)

func (app *App) CorsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		sliceCors := strings.Split(app.config.Cors, ",")
		origin := app.checkCorsHeaders(r, sliceCors)

		writeCorsHeaders(w, origin)

		next.ServeHTTP(w, r)
	})
}

func (app *App) checkCorsHeaders(r *http.Request, cors []string) string {
	var corsHeader string

	origin := r.Header.Get("Origin")

	for i := range cors {
		if strings.Contains(cors[i], origin) {
			corsHeader = cors[i]

			return corsHeader
		}
	}

	return corsHeader
}

func writeCorsHeaders(responseWriter http.ResponseWriter, corsHeader string) {
	responseWriter.Header().Set("Access-Control-Allow-Origin", corsHeader)
	responseWriter.Header().Set("Access-Control-Allow-Credentials", "true")
	responseWriter.Header().Set("Access-Control-Allow-Methods", "GET,POST,PATCH,PUT,UPDATE,DELETE,OPTIONS")
	responseWriter.Header().Set("Access-Control-Allow-Headers", "Content-Type, X-CSRF-Key, Authorization, Cookie")
}

func (app *App) LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if r := recover(); r != nil {
				log.Println(debug.Stack())
			}
		}()
		next.ServeHTTP(w, r)
	})
}
