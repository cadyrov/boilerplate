package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/cadyrov/goerr"
	"golang.org/x/exp/slog"
)

type Error struct {
	Message string            `json:"message"`
	Code    uint              `json:"code"`
	Details map[string]string `json:"details"`
}

type MessageReply struct {
	Message string `json:"message"`
}

func WriteOK(w http.ResponseWriter, status int, in interface{}) {
	bts, em := json.Marshal(in)
	if em != nil {
		w.WriteHeader(http.StatusInternalServerError)

		if _, err := w.Write([]byte(em.Error())); err != nil {
			slog.Error("write ok response", err)
		}

		return
	}

	w.Header().Set("Content-Type", "application/json")

	if _, err := w.Write(bts); err != nil {
		w.WriteHeader(http.StatusInternalServerError)

		slog.Error("write ok response", err)
	}
}

func WriteError(w http.ResponseWriter, e error) {
	err := Error{
		Message: errors.Cause(e).Error(),
	}

	bts, em := json.Marshal(err)
	if em != nil {
		w.WriteHeader(http.StatusInternalServerError)

		if _, err := w.Write([]byte(em.Error())); err != nil {
			slog.Error("write error response", err)
		}

		return
	}

	switch {
	case errors.As(e, &goerr.BadRequest{}):
		w.WriteHeader(http.StatusBadRequest)

		_, _ = w.Write(bts)
	case errors.As(e, &goerr.Unauthorized{}):
		w.WriteHeader(http.StatusUnauthorized)
	case errors.As(e, &goerr.NotFound{}):
		w.WriteHeader(http.StatusNotFound)

		_, _ = w.Write(bts)
	case errors.As(e, &goerr.Conflict{}):
		w.WriteHeader(http.StatusConflict)

		_, _ = w.Write(bts)
	default:
		w.WriteHeader(http.StatusInternalServerError)
	}

	slog.Error("write error response", e)
}
