package handlers

import (
	"net/http"

	"gitlab.com/cadyrov/boilerplate"
)

type SwaggerReply struct{}

// Swagger godoc
// @Summary      получить документацию
// @Description  получить документацию
// @Tags         System
// @Accept       json
// @Produce      json
// @Success      200  {object}  SwaggerReply
// @Failure      500  {object}  Error
// @Router       /system/swagger [GET].
func Swagger() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if _, err := w.Write(boilerplate.Swagger); err != nil {
			WriteError(w, err)
		}
	}
}

type SystemResponse struct {
	Message string `json:"message,omitempty"`
}

// HealthCheck godoc
// @Summary      Проверка сервера
// @Description  Проверка сервера
// @Tags         System
// @Accept       json
// @Produce      json
// @Success      200  {object}  SwaggerReply
// @Router       /system/health [GET].
func HealthCheck() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		WriteOK(w, http.StatusOK, SystemResponse{"Alive"})
	}
}
