package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/cadyrov/boilerplate/api/http/handlers"
)

func (app *ServiceRouter) SetSystemRoutes(router *mux.Router) {
	router.HandleFunc("/swagger", handlers.Swagger()).Methods(http.MethodGet)
	router.HandleFunc("/health", handlers.HealthCheck()).Methods(http.MethodGet)
}
