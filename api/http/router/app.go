package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/cadyrov/boilerplate/api/http/handlers"
)

type ServiceRouter struct {
	*handlers.App
}

func New(app *handlers.App) (*ServiceRouter, error) {
	return &ServiceRouter{
		App: app,
	}, nil
}

func (app *ServiceRouter) Routes() *mux.Router {
	return app.initRoutes()
}

func (app *ServiceRouter) initRoutes() *mux.Router {
	routes := mux.NewRouter()

	routes.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			next.ServeHTTP(w, r)
		})
	})

	routes.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.NotFound(w, r)
	})

	routes.MethodNotAllowedHandler = http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		if _, err := rw.Write([]byte("Method not allowed")); err != nil {
			return
		}
	})

	rootRouter := routes.PathPrefix("/api").Subrouter()
	rootRouter.Use(app.LoggingMiddleware, app.App.CorsMiddleware)

	systemRouter := rootRouter.PathPrefix("/system").Subrouter()
	app.SetSystemRoutes(systemRouter)

	return routes
}
