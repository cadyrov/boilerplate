#!/bin/bash
FROM golang:1.19-alpine as builder

WORKDIR /boilerplate
COPY . /boilerplate
RUN apk add git
RUN apk add curl
RUN apk add make
ENV GO111MODULE=on
RUN go mod vendor
RUN make build

FROM alpine:latest as runner
EXPOSE $PROJECT_HTTP_PORT $PROJECT_METRICS_PORT $PROJECT_GRPC_PORT
COPY --from=builder /boilerplate /boilerplate/
WORKDIR /boilerplate/
ENTRYPOINT ["bin/srv", "run"]